<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Holiday  Packages | Thusharagiri</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="keywords" content="Adventure Tourism , Farm Tourism ,Know the lifestyle of Kerala ">
    <meta name="description" content="Experience Adventure and Farm Tourism">
    <meta name="image" content="http://thusharagiri.in/resources/share.png">
    <meta name="robots" content="index">
    <link rel="canonical" href="http://thusharagiri.in/">
    <meta name="author" content="THUSHARAGIRI EXPLORING">
    <meta property="og:site_name" content="thusharagiri">
    <meta property="og:title" content="Experience Adventure and Farm Tourism">
    <meta property="og:url" content="http://thusharagiri.in/">
    <meta property="og:description" content=" Experience Adventure and Farm Tourism">
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://thusharagiri.in/resources/share.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@">
    <meta name="twitter:title" content="Experience Adventure and Farm Tourism">
    <meta name="twitter:url" content="ttps://thusharagiri.com/">
    <meta name="twitter:description" content="Experience Adventure and Farm Tourism.">
    <meta name="twitter:image" content="http://thusharagiri.in/resources/share.png">
    <!--  / fav-icon  /   -->
     <link rel="apple-touch-icon" sizes="57x57" href="resources/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="resources/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="resources/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="resources/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="resources/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="resources/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="resources/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="resources/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="resources/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="resources/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="resources/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="resources/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="resources/favicon/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <link rel="stylesheet" type="text/css" href="resources/css/style.css">
</head>
<body>
    <!--    / header/ -->
    <?php include 'includes/inner-pages-header.php';?>
        <div class="wrap">
            <section class="main-section show-grid ">
                <h1 class="font-25 mg-tp15 mg-bt45 text-center">Holiday Packages</h1>
                <div class="container">
                    <div class="d-flex row-warp">
                        <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image1.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                    <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>
                           <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image2.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                    <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image3.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                     <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image4.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                     <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>
                                  <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image1.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                     <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>
                           <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image2.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                     <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image3.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                     <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-3">
                            <div class="show-block">
                                <div class="thumbnail">
                                    <img src="resources/images/home/packages/image4.png" />
                                </div>
                                <div class="post-content">
                                   <h4 class="montserrat bold ">Elegant   <span class="fill">Holidays Package</span> </h4>
                                    <div class="price d-flex justify-content-between">
                                        <span>INR 10,500 to 25,000  </span> <span> 5 N / 6 D </span>
                                    </div>
                                    <div class="description">
                                     <p>&nbsp; <!--content here --></p>
                                     <a href="tour-pack.php" class="btn btn-medium ">View more <img src="resources/images/icons/arrow-right.svg"></a>
                                    </div>
                                </div>
                                <div class="grid-overlay"></div>
                            </div>
                        </div>


                    </div>
                </div>
            </section>
        </div>
        <!--    /footer/ -->
        <?php include 'includes/footer.php';?>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="resources/js/hoverIntent.js" type="text/javascript"></script>
<script src="resources/js/superfish.min.js" type="text/javascript"></script>
<script src="resources/js/custom-scripts.js" type="text/javascript"></script>
<script>
     $('#chk_frame').hide;
    $('#hdr_ifr').hide;
var deviceAgent = navigator.userAgent.toLowerCase();
var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
if(agentID) {
    $('.show-block').click(function() {
        var id = $(this).attr('id');
        $(this).find('.grid-overlay').toggleClass('overlay-active');
        $(this).find('.description').stop().animate({
                height: "toggle",
                opacity: "toggle"
            }, 300);
    });
}
else {
    $('.show-block').hover(function() {
            $(this).find('.description').stop().animate({
                height: "toggle",
                opacity: "toggle"
            }, 300);

        });
}
</script>
</html>
