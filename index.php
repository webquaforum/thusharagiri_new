<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Thusharagiri | Experience Adventure and Farm Tourism</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="keywords" content="Adventure Tourism , Farm Tourism ,Know the lifestyle of Kerala ">
    <meta name="description" content="Experience Adventure and Farm Tourism">
    <meta name="image" content="http://thusharagiri.in/resources/share.png">
    <meta name="robots" content="index">
    <link rel="canonical" href="http://thusharagiri.in/">
    <meta name="author" content="THUSHARAGIRI EXPLORING">
    <meta property="og:site_name" content="thusharagiri">
    <meta property="og:title" content="Experience Adventure and Farm Tourism">
    <meta property="og:url" content="http://thusharagiri.in/">
    <meta property="og:description" content=" Experience Adventure and Farm Tourism.">
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://thusharagiri.in/resources/share.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@">
    <meta name="twitter:title" content="Experience Adventure and Farm Tourism">
    <meta name="twitter:url" content="ttps://thusharagiri.com/">
    <meta name="twitter:description" content="Experience Adventure and Farm Tourism.">
    <meta name="twitter:image" content="http://thusharagiri.in/resources/share.png">
    <!--  / fav-icon  /   -->
    <link rel="apple-touch-icon" sizes="57x57" href="resources/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="resources/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="resources/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="resources/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="resources/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="resources/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="resources/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="resources/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="resources/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="resources/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="resources/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="resources/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="resources/favicon/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <link rel="stylesheet" type="text/css" href="resources/css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css">
    <style type="text/css">
        


        .modal {
   display: none;
   position: fixed;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background: rgba(0, 0, 0, 0.77);
   z-index: 999
}
.modal.on {
   display: block
}
.modal-container {
   position: relative;
   display: block;
   top: 10%;
   width: 75%;
   max-width: 75%;
   margin: 0 auto;
   -webkit-animation-name: fadeInDown;
   animation-name: fadeInDown;
   -webkit-animation-duration: 1s;
   animation-duration: 1s;
   -webkit-animation-fill-mode: both;
   animation-fill-mode: both
}
.modal-container .modal-header {
   position: relative;
   display: block;
   border: 1px solid #ddd;
   background: #fff
}
.modal-container .modal-header .modal-title {
   font-size: 30px;
   font-weight: 500;
   line-height: 1.2;
   color: #298619;
   text-align: center;
   padding: 20px 0
}
.modal-container .modal-header span.close {
   position: absolute;
   display: block;
   width: 50px;
   height: 50px;
   background-image: url(resources/images/icons/close.svg);
   top: 0;
   right: 0px;
   cursor: pointer;
   background-color: #298619
}
.modal-container .modal-content {
   position: relative;
   display: block;
   background-color: #fff;
   padding: 25px 45px;
   height: 460px;
   overflow-x: hidden
}
@-webkit-keyframes fadeInDown {
   0% {
       opacity: 0;
       -webkit-transform: translateY(-20px);
       transform: translateY(-20px)
   }
   100% {
       opacity: 1;
       -webkit-transform: translateY(0);
       transform: translateY(0)
   }
}
@keyframes fadeInDown {
   0% {
       opacity: 0;
       -webkit-transform: translateY(-20px);
       transform: translateY(-20px)
   }
   100% {
       opacity: 1;
       -webkit-transform: translateY(0);
       transform: translateY(0)
   }
}
@media (max-width: 768px) {
   .modal-container {
       width: 98%;
       max-width: 98%
   }
   .modal-container .modal-header .modal-title {
       font-size: 21px
   }
   .modal-container .modal-header span.close {
       width: 30px;
       height: 30px
   }
   .modal-container .modal-content {
       padding: 23px 30px;
       max-height: 300px
   }
}
.overflow-stop {
   overflow: hidden !important
}
.overflow-active {
   overflow: visible !important
}
.show-case-grid .caption button {
    display: inline-block;
    padding: 7px;
    color: #fff;
    position: relative;
    opacity: 1;
    cursor: pointer;
    -webkit-transition: all 0.3s cubic-bezier(0.2, 0, 0, 1);
    transition: all 0.3s cubic-bezier(0.2, 0, 0, 1);
    z-index: 1;
    background: #298619;
    border: 0;
    min-width: 130px;
}
    </style>
</head>
<body>
    <!--    / header/ -->
    <?php include 'includes/header.php';?>
        <section class="banner-outer ">
            <div class="hero ">
                <div class="flexslider">
                    <ul class="slides ">
                        <li style="background-image: url(resources/images/home/slide-1.png);">
                            <div class="overlay"></div>
                            <div class="container">
                                <div class="d-flex row-warp align-items-center slider-text-outer ">
                                    <div class="slider-text">
                                        <div class="banner-content d-flex row-warp">
                                            <div class="col-md-6">
                                               <!-- <a href="" class="btn mg-bt35  btn-big">
                                                    <span>Plan your trip now</span> <img src="resources/images/icons/arrow-right-white.svg"> </a> -->
                                            </div>
                                            <div class="col-md-6">
                                                <h1 class="font-75 montserrat ">
                                                        Thusharagiri
                                                        <span class="fill">Experience Adventure and Farm Tourism.</span></h1>
                                                <!--<a class="quick-enq mg-tp25 " href="">Quick enquiry <img src="resources/images/icons/arrow-right-white.svg"></a> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li style="background-image: url(resources/images/home/slide-2.png);">
                            <div class="overlay"></div>
                            <div class="container">
                                <div class="d-flex row-warp align-items-center slider-text-outer ">
                                    <div class="slider-text">
                                        <div class="banner-content d-flex row-warp">
                                            <div class="col-md-6">
                                                <a href="" class="btn mg-bt35  btn-big"><span>Plan your trip now</span> <img src="resources/images/icons/arrow-right-white.svg"> </a>
                                            </div>
                                            <div class="col-md-6">
                                                <h1 class="font-75 montserrat ">
                                                        Thusharagiri
                                                        <span class="fill">Experience Adventure and Farm Tourism.</span></h1>
                                                <a class="quick-enq mg-tp25 " href="">Quick enquiry <img src="resources/images/icons/arrow-right-white.svg"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="main-section">
            <div class="container">
                <div class="row ">
                    <div class="col-md-4">
                        <h2 class="font-50 montserrat regular">
                        Experience 
                        <span class="fill">
                            Thusharagiri.
                        </span>
                    </h2>
                    </div>
                    <div class="col-md-4">
                        <p class="text-justify">Caressed in the folds of the Western Ghats is the picturesque Thusharagiri Waterfalls. Lying 50 km from Kozhikode, the Thusharagiri Waterfalls comprises Erattumukku, Mazhavil Chattom Waterfalls, and Thumbithullum Para which can be reached through trekking. </p>
                    </div>
                    <div class="col-md-4">
                        <p class="text-justify">Thusharagiri or 'mist-capped peaks' gets its name from the beautiful silvery crown formed at the mountain top due to the waterfalls. The nature here will transport to a different dimension where time itself comes to a standstill.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-section video-sec  d-flex justify-content-center flex-column align-items-center" style="background-image:url(resources/images/home/banner-bg.png)">
            <div class="container">
                <div class="d-flex row-warp justify-content-center flex-column align-items-center ">
                    <h2 class="font-50 bold"><a name="our videos">Explore our videos</a></h2>
                    <iframe id="vid_frame" src=" http://www.youtube.com/embed/2rFtfyEgbzU?autoplay=0&amp;rel=0&amp;showinfo=0&amp;autohide=1" frameborder="0"></iframe>
                    <a href="" class="round-btn">
                        <img src="resources/images/icons/arrow-right.svg">
                    </a>
                </div>
            </div>
        </section>
        
        <section class="main-section show-case mg-bt55">
           
            <div class="container">
                <div class="d-flex row-warp">
                    <div class="col-md-6 col-lg-6">
                        <div class="container-modal">
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image5.png" alt="img02">
                            <div class="caption">
                                <h4 class="montserrat bold ">Adventures<span class="fill">In Thusharagiri
                                </span></span></h4>
                               <button class="pop"> Book Now </button> 
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                        <div class="modal " role="alert" id="win-0">
                          <div class="modal-container">
                                    <div class="modal-header">
                                        <div class="modal-title">Adventure tourism and Farm Stay Accomodation</div>
                                        <span class="close"></span>
                                    </div>
                                    <div class="modal-content">
                                        <h4>Hilite farm tourism and adventure tourism</h4>
                                        <ul>
                                            <li>Contact name: Manoj</li>
                                            <li>Contact number: 9562810267</li>
                                            <li>No. Of rooms: 4</li>
                                            <li>Capacity per room : 4 (2 beds)</li>
                                            <li>Rate: non AC 2000</li>
                                            <li>Ac 2500</li>
                                            <li>Location : Thusharagiri</li>
                                            <li>www.thehillranches.com</li>
                                        </ul>
                                        <h4>Home stay </h4>
                                        <ul>
                                            <li> Contact name: James</li>
                                            <li>Contact number: 9447929368</li>
                                            <li>No. Of rooms: 3</li>
                                            <li>Capacity per room :2</li>
                                            <li>Rate:300/day excluding food</li>
                                            <li>Location : Pulikkayam</li>
                                            <li>Mariyan Kodanchery</li>
                                            <li>Contact name: Nikhil</li>
                                            <li>Contact number: 9567676767</li>
                                            <li>No. Of rooms: 12</li>
                                            <li>Capacity per room :2</li>
                                            <li>Rate:650 - 36000</li>
                                            <li>Location : near Kodanchery bus stand</li>
                                            <li>4tune hotel</li>
                                            <li>Contact number: 9745882298 </li>
                                            <li>No. Of rooms: 3 villas </li>
                                            <li>Capacity per room :4 </li>
                                            <li>Rate:2000</li>
                                        </ul>
                                        <h4>Honeyrock resort</h4>
                                        <ul>
                                            <li> Contact name: Rendheep</li>
                                            <li> Contact number: 9072273697</li>
                                            <li> No. Of rooms: 7</li>
                                            <li> Capacity per room : 4</li>
                                            <li> Rate: AC 3500 and non ac 2500 - 3000</li>
                                            <li> Location : Thusharagiri</li>
                                            <li><a target="_blank" href="https://g.co/kgs/KT4C2p">Honeyrock.com</a> </li>
                                        </ul>
                                        <h4>The Hillock resort</h4>
                                        <ul>
                                            <li> Contact name: Rendheep </li>
                                            <li> Contact number: 9072273697 </li>
                                            <li> No. Of rooms: 1 </li>
                                            <li> Capacity per room : 4 </li>
                                            <li> Rate: non AC 3500 </li>
                                            <li> Location : Koorottupara </li>
                                            <li> Puliyilakkattupady </li>
                                            <li> <a target="_blank" href="https://g.co/kgs/koxjE9"> Hillock resort.com</a> </li>
                                            <li> <a target="_blank" href="https://www.facebook.com/Honey-rock-resorts-131232570749383/">Honey-rock-resorts.com</a> </li>
                                        </ul>
                                        <h4>Homestay </h4>
                                        <ul>
                                            <li> Contact name: Raju Sebastian</li>
                                            <li>Contact number: 9995540160</li>
                                            <li>9048405263 (watsapp)</li>
                                            <li>No. Of rooms: 2</li>
                                            <li>Capacity per room : 2</li>
                                            <li>Rate: 800 per head</li>
                                            <li>Location : Vettikavungal House</li>
                                            <li>Opp Gov college Kodenchery</li>
                                        </ul>
                                        <h4> MUKKAM holiday's</h4>
                                        <ul>
                                            <li>D&D adventure club</li>
                                            <li>contact no:+919645412385</li>
                                            <li>+919605177334</li>
                                            <li>Insta/fb- mukkam Holidays</li>
                                            <li> <a target="_blank" href="https://Justriping.com">Justriping.com</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="container-modal">
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/farm-to.jpg" alt="img02">
                            <div class="caption">
                                <h4 class="montserrat bold">Farm stay<span class="fill"> In Thusharagiri</span></span></h4>
                                <button class="pop"> More </button> 
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                         <div class="modal " role="alert" id="win-1">
                                <div class="modal-container">
                                    <div class="modal-header">
                                        <div class="modal-title">Adventure tourism and Farm Stay Accomodation</div>
                                        <span class="close"></span>
                                    </div>
                                    <div class="modal-content">
                                        <h4>Hilite farm tourism and adventure tourism</h4>
                                        <ul>
                                            <li>Contact name: Manoj</li>
                                            <li>Contact number: 9562810267</li>
                                            <li>No. Of rooms: 4</li>
                                            <li>Capacity per room : 4 (2 beds)</li>
                                            <li>Rate: non AC 2000</li>
                                            <li>Ac 2500</li>
                                            <li>Location : Thusharagiri</li>
                                            <li>www.thehillranches.com</li>
                                        </ul>
                                        <h4>Home stay </h4>
                                        <ul>
                                            <li> Contact name: James</li>
                                            <li>Contact number: 9447929368</li>
                                            <li>No. Of rooms: 3</li>
                                            <li>Capacity per room :2</li>
                                            <li>Rate:300/day excluding food</li>
                                            <li>Location : Pulikkayam</li>
                                            <li>Mariyan Kodanchery</li>
                                            <li>Contact name: Nikhil</li>
                                            <li>Contact number: 9567676767</li>
                                            <li>No. Of rooms: 12</li>
                                            <li>Capacity per room :2</li>
                                            <li>Rate:650 - 36000</li>
                                            <li>Location : near Kodanchery bus stand</li>
                                            <li>4tune hotel</li>
                                            <li>Contact number: 9745882298 </li>
                                            <li>No. Of rooms: 3 villas </li>
                                            <li>Capacity per room :4 </li>
                                            <li>Rate:2000</li>
                                        </ul>
                                        <h4>Honeyrock resort</h4>
                                        <ul>
                                            <li> Contact name: Rendheep</li>
                                            <li> Contact number: 9072273697</li>
                                            <li> No. Of rooms: 7</li>
                                            <li> Capacity per room : 4</li>
                                            <li> Rate: AC 3500 and non ac 2500 - 3000</li>
                                            <li> Location : Thusharagiri</li>
                                            <li><a target="_blank" href="https://g.co/kgs/KT4C2p">Honeyrock.com</a> </li>
                                        </ul>
                                        <h4>The Hillock resort</h4>
                                        <ul>
                                            <li> Contact name: Rendheep </li>
                                            <li> Contact number: 9072273697 </li>
                                            <li> No. Of rooms: 1 </li>
                                            <li> Capacity per room : 4 </li>
                                            <li> Rate: non AC 3500 </li>
                                            <li> Location : Koorottupara </li>
                                            <li> Puliyilakkattupady </li>
                                            <li> <a target="_blank" href="https://g.co/kgs/koxjE9"> Hillock resort.com</a> </li>
                                            <li> <a target="_blank" href="https://www.facebook.com/Honey-rock-resorts-131232570749383/">Honey-rock-resorts.com</a> </li>
                                        </ul>
                                        <h4>Homestay </h4>
                                        <ul>
                                            <li> Contact name: Raju Sebastian</li>
                                            <li>Contact number: 9995540160</li>
                                            <li>9048405263 (watsapp)</li>
                                            <li>No. Of rooms: 2</li>
                                            <li>Capacity per room : 2</li>
                                            <li>Rate: 800 per head</li>
                                            <li>Location : Vettikavungal House</li>
                                            <li>Opp Gov college Kodenchery</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                    </div>
                   
                    
               
                </div>
            </div>
        </section>

        <div class="riverfest-intro" style="background-image:url(resources/images/home/ts-bg.png)">
            <div class="dark-bg">
                <div class="col-md-6">
                    <h2>MALABAR 2018 <br> RIVER FESTIVAL  </h2>
                    <a target="_blank" href="http://www.malabarriverfestival.com/">Explore now</a>
                </div>
            </div>
        </div>
        <section class="main-section ">
            <div class="container">
                <div class="d-flex row-warp align-items-center flex-column mg-bt45">
                    <h3 class="font-25 bold text-center"><a name="things">Things to do in  <span class="italics">Thusharagiri</span></a></h3>
		                   <!-- <p class="col-md-9 text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p> -->
                </div>
                <ul class="activites">
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image1.png" alt="img02">
                            <div class="caption">
                                <!--
                                <h4 class="montserrat bold font-20">Walk through  <span class="fill"> the woods</span></h4>
                                <a href=" ">View more</a>
-->
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image2.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Village experiance</h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image3.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">At vero eos et   <span class="fill"> accusamus et</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image4.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Nam libero tempore, <span class="fill"> cum </span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image5.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Temporibus autem  <span class="fill">  quibusdam </span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image6.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Et harum  <span class="fill">quidem rerum</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image7.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Sed ut perspiciatis  <span class="fill">  unde</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image8.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Sed ut perspiciatis  <span class="fill">  unde</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image9.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Sed ut perspiciatis  <span class="fill">  unde</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image10.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Sed ut perspiciatis  <span class="fill">  unde</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                    <li>
                        <div class="show-case-grid">
                            <img src="resources/images/home/activites/image11.png" alt="img02">
                            <!--
                            <div class="caption">
                                <h4 class="montserrat bold font-20">Sed ut perspiciatis  <span class="fill">  unde</span></h4>
                                <a href=" ">View more</a>
                            </div>
-->
                            <div class="grid-overlay"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
		
		<section class="main-section show-grid pad-0 ">
                <div class="container">
                    <div class=" d-flex  flex-column align-items-center text-center">
                        <h1 class="font-25"><a name="contact">Contact Us </a> </h1>
                        
                    </div>
                    <div class="d-flex  mg-bt55 mg-tp45 row-warp align-items-center justify-content-center">
                       
                        <div class="col-lg-8 ">
                            <h4 class="montserrat" style="margin-left:15px;">How can we help you?</h4>
                            <form id="contactForm">
                                <div class=" form-container">
                                    <div class="d-flex row-warp">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input class="form-control placeholder" placeholder=" Name" required="" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input class="form-control placeholder " placeholder="Email" required="" autocomplete="off" type="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex row-warp">
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <input class="form-control placeholder" placeholder="Mobile Number" required="" pattern="[0-9]*" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <input class="form-control placeholder" placeholder="Subject" autocomplete="off" required=""  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex  row-warp">
                                        <div class="col-md-12  ">
                                            <div class="form-group">
                                                <textarea class="form-control  text-area placeholder" required="" placeholder=" Message" rows="4" autocomplete="off"></textarea>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <button type="submit" class="btn">Submit <img src="resources/images/icons/arrow-right-white.svg"> </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
		
		
		
        <!--
        <section class="main-section show-case mg-bt60">
            <div class="bg-overlay">
                <h3>Coming soon </h3>
            </div>
            <div class="container">
                <div class="d-flex row-warp">
                    <div class="col-md-4 col-lg-3">
                        <h3 class="mg-tp25 font-25 bold ">Tour packages in  <span class="italics"> Thusharagiri</span></h3>
                        <p class="mg-tp20">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            <br>
                            <br> Lorem Ipsum has been the industry's standard dummy text.</p>
                        <a href="" class="btn mg-bt35">See more <img src="resources/images/icons/arrow-right-white.svg"> </a>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image1.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold ">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href="">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image2.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href=" ">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image3.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href=" ">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image4.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href=" ">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image5.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href=" ">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image6.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href=" ">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="show-case-grid">
                            <img src="resources/images/home/packages/image7.png" alt=" ">
                            <div class="caption">
                                <h4 class="montserrat bold">Rain  <span>Country <span class="fill"> Resorts</span></span></h4>
                                <p>INR 10,500 to 25,000</p>
                                <a href=" ">View more</a>
                            </div>
                            <div class="grid-overlay"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
-->
        <!--    /footer/ -->
        <?php include 'includes/footer.php';?>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="resources/js/hoverIntent.js" type="text/javascript"></script>
<script src="resources/js/superfish.min.js" type="text/javascript"></script>
<script src="resources/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="resources/js/custom-scripts.js" type="text/javascript"></script>
<script>
    $('#chk_frame').hide;
    $('#hdr_ifr').hide;
    var sliderMain = function() {
        $('.hero .flexslider').flexslider({
            animation: "fade",
            //easing: "swing",
            // direction: "vertical",
            slideshowSpeed: 7000,
            directionNav: true,
            start: function() {
                setTimeout(function() {
                    $('.slider-text').removeClass('animated fadeInUp');
                    $('.flex-active-slide').find('.slider-text').addClass('animated fadeInUp');
                }, 600);
            },
            before: function() {
                setTimeout(function() {
                    $('.slider-text').removeClass('animated fadeInUp');
                    $('.flex-active-slide').find('.slider-text').addClass('animated fadeInUp');
                }, 600);
            }
        });
    };
    $(function() {
        sliderMain();
    });
    $('.container-modal .pop').each(function(idx, item) {
        var winnerId = "winner-" + idx;
        this.id = winnerId;
        $(this).click(function() {
            var btn = $("#winner-" + idx);
            var span = $(".close");
            var popId = $('#win-' + idx);
            btn.click(function() {
                $(popId).addClass('on');
                $('body').addClass('overflow-stop').removeClass('overflow-active')
            });
            span.click(function() {
                $(popId).removeClass('on');
                $('body').addClass('overflow-active').removeClass('overflow-stop');
            });
        });
    });
</script>
</html>
