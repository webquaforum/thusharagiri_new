<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gallery | Thusharagiri</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="keywords" content="Adventure Tourism , Farm Tourism ,Know the lifestyle of Kerala ">
    <meta name="description" content="Experience Adventure and Farm Tourism">
    <meta name="image" content="http://thusharagiri.in/resources/share.png">
    <meta name="robots" content="index">
    <link rel="canonical" href="http://thusharagiri.in/">
    <meta name="author" content="THUSHARAGIRI EXPLORING">
    <meta property="og:site_name" content="thusharagiri">
    <meta property="og:title" content="Experience Adventure and Farm Tourism">
    <meta property="og:url" content="http://thusharagiri.in/">
    <meta property="og:description" content=" Experience Adventure and Farm Tourism">
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://thusharagiri.in/resources/share.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@">
    <meta name="twitter:title" content="Experience Adventure and Farm Tourism">
    <meta name="twitter:url" content="ttps://thusharagiri.com/">
    <meta name="twitter:description" content="Experience Adventure and Farm Tourism.">
    <meta name="twitter:image" content="http://thusharagiri.in/resources/share.png">
    <!--  / fav-icon  /   -->
     <link rel="apple-touch-icon" sizes="57x57" href="resources/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="resources/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="resources/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="resources/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="resources/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="resources/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="resources/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="resources/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="resources/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="resources/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="resources/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="resources/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="resources/favicon/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <link rel="stylesheet" type="text/css" href="resources/css/darkbox.css">
    <link rel="stylesheet" type="text/css" href="resources/css/fancybox.css">
    <link rel="stylesheet" type="text/css" href="resources/css/style.css">
</head>
<body>
    <!--    / header/ -->
    <?php include 'includes/inner-pages-header.php';?>
        <div class="wrap">
            <section class="main-section show-grid ">
                <div class="container">
                    <div class="d-flex  flex-column align-items-center gal-container">
                        <h1 class="font-25">Explore Our Gallery </h1>
                        <p><!-- content here --></p>
                        <div class="gallery fill d-flex justify-content-center flex-column mg-tp45">
                            <div class="gallery-menu mg-bt35">
                                <ul>
                                    <li><a href="#content1">Photo Gallery</a></li>
                                    <li><a href="#content2">Video Gallery</a></li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                            <div id="content1" class="gallery-content">
                                <div class="gal-inner d-flex row-warp">
                                    <div class="col-md-4 col-lg-3">
                                        <div class="gallery-item">
                                            <div class="thumb ">
                                                <img src="resources/images/gallery/thumb/img-1.png" data-darkbox="resources/images/gallery/img-1.png" data-darkbox-group="two" data-darkbox-description="Experience Adventure and Farm Tourism.">
                                            </div>
                                            <div class="gal-title">
                                                 <p><!-- content here --></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3">
                                        <div class="gallery-item">
                                            <div class="thumb ">
                                                <img src="resources/images/gallery/thumb/img-2.png" data-darkbox="resources/images/gallery/img-2.png" data-darkbox-group="two" data-darkbox-description="Experience Adventure and Farm Tourism.">
                                            </div>
                                            <div class="gal-title">
                                                 <p><!-- content here --></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3">
                                        <div class="gallery-item">
                                            <div class="thumb ">
                                                <img src="resources/images/gallery/thumb/img-3.png" data-darkbox="resources/images/gallery/img-3.png" data-darkbox-group="two" data-darkbox-description="Experience Adventure and Farm Tourism.">
                                            </div>
                                            <div class="gal-title">
                                                 <p><!-- content here --></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3">
                                        <div class="gallery-item">
                                            <div class="thumb ">
                                                <img src="resources/images/gallery/thumb/img-4.png" data-darkbox="resources/images/gallery/img-4.png" data-darkbox-group="two" data-darkbox-description="Experience Adventure and Farm Tourism.">
                                            </div>
                                            <div class="gal-title">
                                                <p><!-- content here --></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="content2" class="gallery-content">
                                <div class="gal-inner d-flex row-warp">
                                    <div class="col-md-4 col-lg-3">
                                        <div class="gallery-item">
                                            <div class="thumb ">
                                                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/FLpoYIG043A?autoplay=1&wmode=opaque"><img src="https://img.youtube.com/vi/FLpoYIG043A/hqdefault.jpg"> </a>
                                            </div>
                                            <div class="gal-title">
                                                <p><!-- content here --></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-3">
                                        <div class="gallery-item">
                                            <div class="thumb ">
                                                <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/9aJkiMXJEf4?autoplay=1&wmode=opaque"><img src="https://img.youtube.com/vi/9aJkiMXJEf4/hqdefault.jpg"> </a>
                                            </div>
                                            <div class="gal-title">
                                                <p><!-- content here --></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--    /footer/ -->
        <?php include 'includes/footer.php';?>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="resources/js/hoverIntent.js" type="text/javascript"></script>
<script src="resources/js/superfish.min.js" type="text/javascript"></script>
<script src="resources/js/darkbox.js" type="text/javascript"></script>
<script src="resources/js/fancybox.js" type="text/javascript"></script>
<script src="resources/js/custom-scripts.js" type="text/javascript"></script>
<script>
     $('#chk_frame').hide;
    $('#hdr_ifr').hide;
    function tab(el) {
        el.find("ul").each(function() {
            var $active, $content;
            var $links = $(this).find("a");
            $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
            $active.addClass("active");
            $content = $($active[0].hash);
            $content.fadeIn("slow");
            $links.not($active).each(function() {
                $(this.hash).hide();
            });
            $(this).on('click', 'a', function(i) {
                $active.removeClass('active');
                $content.hide();
                $active = $(this);
                $content = $(this.hash);
                $active.addClass('active');
                $content.fadeIn("slow");
                i.preventDefault();
            });
        });
    }
    $(document).ready(function() {
        tab($(".gallery-menu"));
    });
    $('body').on('click', '.gallery-pop:not(> img)', function() {
        $('.gallery-pop').remove();
    });
    $(".fancybox").attr('rel', 'gallery-pop').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        nextEffect: 'none',
        prevEffect: 'none',
        padding: 0,
        margin: [20, 60, 20, 60] // Increase left/right margin
    });
</script>
</html>
