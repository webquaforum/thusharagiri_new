<!DOCTYPE html>
<html>

<head>
    <title>Contact | Thusharagiri</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="keywords" content="Adventure Tourism , Farm Tourism ,Know the lifestyle of Kerala ">
    <meta name="description" content="Experience Adventure and Farm Tourism">
    <meta name="image" content="http://thusharagiri.in/resources/share.png">
    <meta name="robots" content="index">
    <link rel="canonical" href="http://thusharagiri.in/">
    <meta name="author" content="THUSHARAGIRI EXPLORING">
    <meta property="og:site_name" content="thusharagiri">
    <meta property="og:title" content="Experience Adventure and Farm Tourism">
    <meta property="og:url" content="http://thusharagiri.in/">
    <meta property="og:description" content=" Experience Adventure and Farm Tourism">
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://thusharagiri.in/resources/share.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@">
    <meta name="twitter:title" content="Experience Adventure and Farm Tourism">
    <meta name="twitter:url" content="ttps://thusharagiri.com/">
    <meta name="twitter:description" content="Experience Adventure and Farm Tourism.">
    <meta name="twitter:image" content="http://thusharagiri.in/resources/share.png">
    <!--  / fav-icon  /   -->
     <link rel="apple-touch-icon" sizes="57x57" href="resources/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="resources/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="resources/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="resources/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="resources/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="resources/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="resources/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="resources/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="resources/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="resources/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="resources/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="resources/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="resources/favicon/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <!-- / css / -->
    <link rel="stylesheet" type="text/css" href="resources/css/style.css">
</head>

<body onload="initialize()">
    <!--    / header/ -->
    <?php include 'includes/inner-pages-header.php';?>
        <div class="wrap">
            <section class="main-section show-grid ">
                <div class="container">
                    <div class="btm-bdr d-flex  flex-column align-items-center text-center">
                        <h1 class="font-25">Contact Us </h1>
                        <p class="fill-75"> <!-- content here --> </p>
                    </div>
                    <div class="d-flex  mg-bt55 mg-tp45 row-warp">
                        <div class="col-lg-4">
                            <div class="contact-info">
                                <h4 class="montserrat">Contact Information</h4>
                                <ul class="contact-info">
                                    <li>
                                        <span class="fill">Thusharagiri Holidays (India) Pvt. Ltd.</span>
                                        <span class="fill">Thusharagiri,Kozhikode</span>
                                        <span class="fill">Kerala, India </span>
                                        
                                    </li>
                                    <li>
                                        <a href="tel:+"> <span class="semi-bold">Tel: </span> +91 9876543210, 7456321, 9876543 </a>
                                    </li>
                                    <li>
                                        <a href="tel:+"> <span class="semi-bold">Mobile: </span> +91 945 88 888 88 </a>
                                    </li>
                                    <li><a href="info@thusharagiri.in"><span class="semi-bold">Email:</span> info@thusharagiri.in </a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-8 ">
                            <h4 class="montserrat" style="margin-left:15px;">How can we help you?</h4>
                            <form id="contactForm">
                                <div class=" form-container">
                                    <div class="d-flex row-warp">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input class="form-control placeholder" placeholder=" Name" required="" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input class="form-control placeholder " placeholder="Email" required="" autocomplete="off" type="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex row-warp">
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <input class="form-control placeholder" placeholder="Mobile Number" required="" pattern="[0-9]*" autocomplete="off" type="text">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <input class="form-control placeholder" placeholder="Subject" autocomplete="off" required=""  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex  row-warp">
                                        <div class="col-md-12  ">
                                            <div class="form-group">
                                                <textarea class="form-control  text-area placeholder" required="" placeholder=" Message" rows="4" autocomplete="off"></textarea>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                <button type="submit" class="btn">Submit <img src="resources/images/icons/arrow-right-white.svg"> </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                     <div id="map-canvas" class="mg-tp45"></div>
                </div>
            </section>
        </div>
        <!--    /footer/ -->
        <?php include 'includes/footer.php';?>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="resources/js/hoverIntent.js" type="text/javascript"></script>
<script src="resources/js/superfish.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgxY6DqJ4TxnRfKjlZR8SfLSQRtOSTxEU"></script>
<script src="resources/js/custom-scripts.js" type="text/javascript"></script>
<script>
    $('#chk_frame').hide;
    $('#hdr_ifr').hide;
  //custom select
        function initialize() {
            var mapOptions = {
                zoom: 13,
                center: new google.maps.LatLng(11.4730785,76.0520509)
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(11.4730785,76.0520509),
                icon: 'resources/images/icons/marker.png'
            });
        };
</script>
</html>
