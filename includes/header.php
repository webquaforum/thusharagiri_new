<div class="header-wrapper">
    <div id="preloader">
        <div class="vertical-centered-box">
            <div class="loader-content">
                <div class="loader-circle"></div>
                <div class="loader-line-mask">
                    <div class="loader-line"></div>
                </div>
            </div>
            <img src="resources/images/logo.png" alt="logo">
        </div>
    </div>
    <header class="header" id="home">
        <button type="button" id="mobile-nav-toggle">
            <i class=" lnr-menu">
                          <span></span>
                          <span></span>
                          <span></span>
                      </i>
        </button>
        <div class="container">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">
                    <a href="index.php"><img src="resources/images/logo.png"> </a>
                </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                         <li><a href="index.php">Home</a></li>
                        <li><a href="#our videos">Our videos</a></li>
                        <li><a href="#things"> Things to do </a></li> 
						<li><a href="#contact"> Contact </a></li>
                    </ul>
                </nav>
                <!-- #nav-menu-container -->
            </div>
        </div>
    </header>
   
</div>
