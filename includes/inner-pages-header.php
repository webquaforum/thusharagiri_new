<div class="header-wrapper">
    <div id="preloader">
        <div class="vertical-centered-box">
            <div class="loader-content">
                <div class="loader-circle"></div>
                <div class="loader-line-mask">
                    <div class="loader-line"></div>
                </div>
            </div>
            <img src="resources/images/logo.png" alt="logo">
        </div>
    </div>
    <header class="header view-pages">
        <button type="button" id="mobile-nav-toggle">
            <i class=" lnr-menu">
                          <span></span>
                          <span></span>
                          <span></span>
                      </i>
        </button>
        <div class="container">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">
                    <a href="index.php"><img src="resources/images/logo.png"> </a>
                </div>
               <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="tour-packages.php">Tour packages</a></li>
                        <li><a href="holiday-packages.php"> Holiday packages </a></li>
                        <li><a href="#"> Things to do </a></li> 
						<li><a href="gallery.php"> Gallery </a></li>
						<li><a href="contact.php"> Contact </a></li>
                    </ul>
                </nav> 
                <!-- #nav-menu-container -->
            </div>
        </div>
    </header>
    <div class="menu d-flex flex-column align-items-center justify-content-start  menu-mm trans_400">
        <div class="menu-close-container">
            <div class="menu-close">
                <div></div>
                <div></div>
            </div>
        </div>
        <nav class="menu-nav">
            <ul>
                <li class="menu-active"><a href="">Home</a></li>
                <li><a href="">Tour packages</a></li>
                <li><a href=""> Holyday packages </a></li>
                <li><a href=""> Things to do </a></li>
                <li><a href="">Lodging</a></li>
            </ul>
        </nav>
    </div>
</div>
