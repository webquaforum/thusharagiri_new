
<footer class="footer">
    <div class="container">
        <div class="d-flex row-warp">
            <div class="col-md-4">
                <h4 class="ex-bold montserrat">Thusharagiri.in</h4>
                <ul class="address">
                    <li>
                        <span> Thusharagiri Holidays (India) Pvt. Ltd.</span><br>
                        <span> Thusharagiri, Kozhikode </span><br>
						<span> Kerala, India </span>
                    </li>
                    <li>Tel: +91 9876543210, 7456321, 9876543</li>
                    <li><a href="">Mob: +91 944 88 888 88  </a></li>
                    <li><a href="">info@thusharagiri.in</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4 class="ex-bold montserrat">Quick Links</h4>
                <ul class="quick-links">
                    <li><a href="reach.php">How to get there ? </a></li>
                    <li><a href="">Travel Care </a></li>
                    <li><a href="">Service Providers  </a></li>
                    <li><a href="">How to?   </a></li>
                    <li><a href="">News   </a></li>
                    <li><a href="">Blog   </a></li>
                </ul>
            </div>
            <div class="col-md-3 social-feeds">
                <img src="resources/images/feeding/fb-feed.png">
                <img src="resources/images/feeding/g-plus-feed.png">

            </div>
            <div class="col-md-3">
                <h4 class="ex-bold montserrat">Subscribe our newsletter</h4>

                <form class="chat">
                    <input placeholder="Enter your mail id" required="" type="email">
                    <button class="montserrat ex-bold">Subscribe</button>
                </form>
                <div class="social-connect d-flex row-warp justify-content-between">
                    <h4 class="ex-bold montserrat">Follow us</h4>
                    <ul class=" d-flex row-warp">
                        <li><a href="#" title="Facebook"><i class=" facebook"><img src="resources/images/icons/fb.svg"> </i></a></li>
                        <li><a href="#" title="Twitter"><i class="twitter"><img src="resources/images/icons/twitter.svg"></i></a></li>
                        <li><a href="#" title="Google +"><i class="fgoogle-plus"> <img src="resources/images/icons/google-plus.svg"> </i></a></li>
                        <li><a href="#" title="insta"><i class="instagram"><img src="resources/images/icons/instagram.svg"></i></a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="copy-right">
        <p>Copyright @ 2018. All rights reserved</p>
    </div>
</footer>
